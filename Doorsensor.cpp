#include "application.h"
#line 1 "c:/Users/gaspa/Documents/Particle/projects/part/src/part.ino"
/*
 * Project part
 * Description: Door Sensor testing
 * Author:FElix Apaza
 * Date:May 6, 2019
 */
void setup();
void loop();
#line 7 "c:/Users/gaspa/Documents/Particle/projects/part/src/part.ino"
int led1 = D7;
  int door1 = D4;
  int ir = D2;
  int led2 = D0;
  int state;
  int irstate;
// setup() runs once, when the device is first turned on.
void setup() {

  // Put initialization like pinMode and begin functions here.
    pinMode(led1,OUTPUT);
    pinMode(led2,OUTPUT);
    pinMode(ir,INPUT);
    pinMode(door1,INPUT);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // The core of your code will likely live here

   irstate = digitalRead(ir);
   state = digitalRead(door1);
    if(state == LOW)
    {
        digitalWrite(led1, LOW);
    }
    else{
        digitalWrite(led1,HIGH);
    }
    if(irstate == HIGH)
    {
        digitalWrite(led2, HIGH);
    }
    else{
        digitalWrite(led2,LOW);
    }
}

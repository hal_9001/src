#include "application.h"
#line 1 "c:/Users/gaspa/Documents/Particle/projects/part/src/part.ino"
/*
 * Project part
 * Description: Door Sensor testing
 * Author:FElix Apaza
 * Date:May 6, 2019
 */
#include <MQTT.h>
void setup();
void loop();
#line 8 "c:/Users/gaspa/Documents/Particle/projects/part/src/part.ino"
void callback(char* topic, byte* payload, unsigned int length); //Needed for MQTT

MQTT client("192.168.2.2", 1883, callback); //Needed for MQTT this IP and port number will work with my current MQTT server
String SSID; //Needed for MQTT, will be set to match my server
String password; //Needed for MQTT, will be set to match my server
SYSTEM_MODE(SEMI_AUTOMATIC);//Needed for MQTT, This will allow the photon to work if no internet is connect to network
SYSTEM_THREAD(ENABLED); //Needed for MQTT, goes with previous part
String dname = "motiondoor.local"; //This value must be different for each device on the MQTT server <-----

  
  int led1 = D7;
  int door1 = D4;
  int ir = D2;
  int led2 = D0;
  int state;
  int irstate;
// setup() runs once, when the device is first turned on.
void setup() {

  // Put initialization like pinMode and begin functions here.
    pinMode(led1,OUTPUT);
    pinMode(led2,OUTPUT);
    pinMode(ir,INPUT);
    pinMode(door1,INPUT);
    SSID = "FinalP"; //Needed for MQTT, this is SSID for server network
 password = "password"; //Needed for MQTT, This is password for network server is on
 WiFi.on(); //Needed for MQTT
 WiFi.disconnect(); //Needed for MQTT
 WiFi.clearCredentials(); //Needed for MQTT
 WiFi.setCredentials(SSID, password, WPA2, WLAN_CIPHER_AES); //Needed for MQTT
 WiFi.connect(); //Needed for MQTT
 waitUntil(WiFi.ready); //Needed for MQTT
 client.connect(dname); //Needed for MQTT
 
        //client.subscribe("Lights"); //Needed for MQTT, this command sets the name of the MQTT topic you are subscribing too. Just ask what to put here
      
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // The core of your code will likely live here

   irstate = digitalRead(ir);
   state = digitalRead(door1);
    if(state == LOW)
    {
        digitalWrite(led1, LOW);
    }
    else{
        client.publish("door","1");
        digitalWrite(led1,HIGH);
        delay(1000);
    }
    if(irstate == HIGH)
    {
        client.publish("motion","1");
        digitalWrite(led2, HIGH);
        delay(1000);
    }
    else{
        digitalWrite(led2,LOW);
    }
}

void callback(char* topic, byte* payload, unsigned int length) {
    char p[length + 1];
    memcpy(p, payload, length);
    
}
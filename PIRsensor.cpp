#include "application.h"
#line 1 "c:/Users/gaspa/Documents/Particle/projects/Pirsensor/PIR/src/PIR.ino"
/*
 * Project PIR
 * Description:PIR sensor
 * Author: Felix Apaza
 * Date:May 6, 2019
 */

// setup() runs once, when the device is first turned on.
void setup();
void loop();
#line 9 "c:/Users/gaspa/Documents/Particle/projects/Pirsensor/PIR/src/PIR.ino"
SYSTEM_MODE(MANUAL);
int led1 = D1;
int ir = D4;
int irstate;
// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.
    pinMode(ir,INPUT);
    pinMode(led1,OUTPUT);
    digitalWrite(led1, LOW);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // The core of your code will likely live here.
  irstate = digitalRead(ir);
  digitalWrite(led1, irstate);  
  
}
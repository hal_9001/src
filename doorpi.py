import RPi.GPIO as GPIO
import time
import sys
import signal 

# Set Broadcom mode so we can address GPIO pins by number.
GPIO.setmode(GPIO.BCM) 

# This is the GPIO pin number we have one of the door sensor
# wires attached to, the other should be attached to a ground pin.DOOR_SENSOR_PIN = 18
DOOR_SENSOR_PIN = 18
# These are the GPIO pin numbers we have the lights attached to
SENSOR = 9

# Initially we don't know if the door sensor is open or closed...
isOpen = None
oldIsOpen = None 

# Clean up when the user exits with keyboard interrupt
def cleanup(signal, frame): 
    GPIO.output(SENSOR, False) 
    GPIO.cleanup() 
    sys.exit(0)

# Set up the door sensor pin.
GPIO.setup(DOOR_SENSOR_PIN, GPIO.IN, pull_up_down = GPIO.PUD_UP) 


GPIO.setup(SENSOR, GPIO.OUT) 


GPIO.output(SENSOR, False) 

# Set the cleanup handler for when user hits Ctrl-C to exit
signal.signal(signal.SIGINT, cleanup) 

while True: 
    oldIsOpen = isOpen 
    isOpen = GPIO.input(DOOR_SENSOR_PIN) 
    GPIO.output(SENSOR, True)  
    time.sleep(0.1)
